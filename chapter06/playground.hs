factorial 0 = 1
factorial n = n * factorial (n - 1)

prod [] = 1
prod (n : ns) = n * product ns

len [] = 0
len (x : xs) = 1 + len xs

rev [] = []
rev (x : xs) = rev xs ++ [x]

insert x [] = [x]
insert x (y : l)
  | x <= y = x : y : l
  | otherwise = y : insert x l

isort [] = []
isort (x : xs) = insert x (isort xs)

myZip [] _ = []
myZip _ [] = []
myZip (x : xs) (y : ys) = (x, y) : zip xs ys

myDrop 0 xs = xs
myDrop n [] = []
myDrop n (x : xs) = drop (n - 1) xs

fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n - 2) + fibonacci (n - 1)

qsort [] = []
qsort (x : xs) = qsort smaller ++ [x] ++ qsort larger
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

myEven 0 = True
myEven n = myOdd (n - 1)

myOdd 0 = False
myOdd n = myEven (n - 1)

-- 6.8 exercise
-- 1
myPow _ 0 = 1
myPow b n = b * myPow b (n - 1)

-- 3
myAnd [] = True
myAnd (b : bs)
  | b == True = myAnd bs
  | otherwise = False

myConcat [] = []
myConcat [l] = l
myConcat (l : ls) = l ++ (myConcat ls)

myReplicate 0 x = []
myReplicate n x = x : myReplicate (n - 1) x

myNth 0 (x : xs) = x
myNth n (x : xs) = myNth (n - 1) xs

myElem x [] = False
myElem x (y : l)
  | x == y = True
  | otherwise = myElem x l

merge xs [] = xs
merge [] ys = ys
merge (x : xs) (y : ys)
  | x <= y = x : merge xs (y : ys)
  | x >  y = y : merge (x : xs) ys

halve xs = (take n xs, drop n xs)
    where n = div (length xs) 2

msort [] = []
msort [x] = [x]
msort l = merge (fst halved) (snd halved)
    where halved = halve l

-- 6
mySum [] = 0
mySum (x : xs) = x + mySum xs

myTake 0 xs = []
myTake n [] = []
myTake n (x : xs) = x : myTake (- n 1) xs

myLast [x] = x
myLast (x : xs) = myLast xs
