double x = x + x
-- double 3
-- double (double 2)
-- sum [1..5]
-- [1..5]

my_sum [] = 0
my_sum (x:xs) = x + sum xs

-- :t my_sum
-- my_sum :: Num p => [p] -> p

-- my_sum [1..10]

qsort [] = []
qsort (x:xs) = qsort smaller ++ [x] ++ qsort larger
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]


-- :t qsort
-- qsort :: Ord a => [a] -> [a]
-- qsort [2,6,4,1,5]

-- N = a / (length xs)
--     where
--         a = 10
--         xs = [1..5]
