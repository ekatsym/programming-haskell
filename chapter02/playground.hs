my_last (x:[]) = x
my_last (x0:x1:xs) = my_last (x1:xs)

-- my_last [0]
-- my_last [0..100]

my_init [] = []
my_init (x:[]) = []
my_init (x0:x1:xs) = x0:(my_init (x1:xs))

-- my_init []
-- my_init [0]
-- my_init [0..100]

