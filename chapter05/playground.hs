[x | x <- [1..10], even x]

factors n = [x | x <- [1..n], mod n x == 0]
prime n = factors n == [1, n]
primes n = [x | x <- [1..n], prime x]

primes 100

prime 21

find k t = [v | (k', v) <- t, k == k']

find 'b' [('a', 1), ('b', 2), ('c', 3), ('d', 4)]

pairs xs = zip xs (tail xs)

sorted xs = and [x <= y | (x, y) <- pairs xs]

positions x xs = [i | (x', i) <- zip xs [0..n], x == x']
    where n = length xs - 1

"abcde" !! 2
take 3 "abcdefg"


-- 5.7 exercise

-- 1
sum [x ^ 1 | x <- [1 .. 100]]

-- 2
replicate n x = [x | i <- [1 .. n]]

-- 3
pyths n = [(x, y, z) | x <- [1 .. n], y <- [1 .. n], z <- [1 .. n], (x ^ 2) + (y ^ 2) == (z ^ 2)]

-- 4
perfects n = [n' | n' <- [1..n], sum [n'' | n'' <- [1..n'-1], mod n' n'' == 0] == n']

-- 5
concat [[(x, y) | y <- [4, 5, 6]] | x <- [1, 2, 3]]

-- 6
find k t = [v | (k', v) <- t, k == k']
positions x xs = find x (zip xs [0 .. n])
    where n = length xs - 1

-- 7
scalarproduct l1 l2 = sum [x1 * x2 | (x1, x2) <- zip l1 l2]
