myAbs n = if n >= 0 then n else -n

mySignum n
  | n < 0 = -1
  | n == 0 = 0
  | otherwise = 1

isDigit c = c >= '0' && c <= '9'

myEven n = mod n 2 == 0

mySplitAt n xs = (take n xs, drop n xs)

myRecip n = 1 / n

myFst (x, _) = x

mySnd (_, y) = y

halve [] = ([], [])
halve xs = (take n xs, drop n xs)
    where
        n = length xs / 2

safetail xs
  | [] = []
  | x : l = l


tail [0..9]
