add x y = x + y

twice f x = f (f x)

myMap1 f [] = []
myMap1 f (x:xs) = f x : myMap1 f xs

myMap2 f xs = [f x | x <- xs]

myFilter1 p [] = []
myFilter1 p (x:xs)
  | y == True = x : myFilter1 p xs
  | y == False = myFilter1 p xs
  where y = p x

myFilter2 p xs = [x | x <- xs, p x == True]

sumsqreven ns = sum (map (^2) (filter even ns))

sumsqreven [1..10]

snoc x xs = xs ++ [x]

compose = foldr (.) id


:type foldr
:type (++)
:type snoc


-- 7.8 exercise
-- 1
map f (filter p xs)

-- 2
myAll p [] = True
myAll p (x:xs)
  | p x = myAll p xs
  | otherwise = False

myAny p [] = False
myAny p (x:xs)
  | p x = True
  | otherwise = myAny p xs

myTakeWhile p [] = []
myTakeWhile p (x:xs)
  | p x = x : myTakeWhile p xs
  | otherwise = []

myDropWhile p [] = []
myDropWhile p (x:xs)
  | p x = myDropWhile p xs
  | otherwise = xs

-- 3
myMap3 f = foldr (\ x acc -> f x : acc) []

myFilter3 p = foldr (\ x acc -> if p x then x : acc else acc) []

-- 4
dec2int = foldl (\ acc x -> x + 10 * acc) 0

-- 6
myCurry f = \ x -> (\ y -> f(x,y))
myUncurry f = \ (x,y) -> f x y

-- 7
myUnfold p h t x
  | p x = []
  | otherwise = h x : unfold p h t (t x)
