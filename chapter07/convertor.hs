type Bit = Int

bin2int = foldr (\ x y -> x + 2 * y) 0

bin2int [] = 0
bin2int (x:xs) = x + 2 * bin2int xs

int2bin 0 = []
int2bin n = mod n 2 : int2bin (div n 2)

make8 bits = take 8 (bits ++ repeat 0)

encode = concat . map (make8 . int2bin . ord)

chop8 [] = []
chop8 bits = take 8 bits : chop8 (drop 8 bits)

decode = map (chr . bin2int) . chop8

transmit = decode . channel . encode

channel = id
